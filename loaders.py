import pickle
import os
import cv2

from keras.datasets import mnist, cifar100, cifar10
from keras.preprocessing.image import ImageDataGenerator, load_img, save_img, img_to_array

import pandas as pd

import numpy as np
from os import walk, getcwd
import h5py
import tensorflow as tf
import matplotlib.pyplot as plt

import scipy
from glob import glob
import shutil
import errno

from keras.applications import vgg19
from keras import backend as K
from keras.utils import to_categorical

import pdb


class ImageLabelLoader():
    def __init__(self, image_folder, target_size):
        self.image_folder = image_folder
        self.target_size = target_size

    def build(self, att, batch_size, label=None):

        data_gen = ImageDataGenerator(rescale=1. / 255)
        if label:
            data_flow = data_gen.flow_from_dataframe(
                att
                , self.image_folder
                , x_col='image_id'
                , y_col=label
                , target_size=self.target_size
                , class_mode='other'
                , batch_size=batch_size
                , shuffle=True
            )
        else:
            data_flow = data_gen.flow_from_dataframe(
                att
                , self.image_folder
                , x_col='image_id'
                , target_size=self.target_size
                , class_mode='input'
                , batch_size=batch_size
                , shuffle=True
            )

        return data_flow


class DataLoader():
    def __init__(self, dataset_name, img_res=(256, 256)):
        self.dataset_name = dataset_name
        self.img_res = img_res

    def load_data(self, domain, batch_size=1, is_testing=False):
        data_type = "train%s" % domain if not is_testing else "test%s" % domain
        path = glob('./data/%s/%s/*' % (self.dataset_name, data_type))

        batch_images = np.random.choice(path, size=batch_size)

        imgs = []
        for img_path in batch_images:
            img = self.imread(img_path)
            if not is_testing:
                img = scipy.misc.imresize(img, self.img_res)

                if np.random.random() > 0.5:
                    img = np.fliplr(img)
            else:
                img = scipy.misc.imresize(img, self.img_res)
            imgs.append(img)

        imgs = np.array(imgs) / 127.5 - 1.

        return imgs

    def load_batch(self, batch_size=1, is_testing=False):
        data_type = "train" if not is_testing else "val"
        path_A = glob('./data/%s/%sA/*' % (self.dataset_name, data_type))
        path_B = glob('./data/%s/%sB/*' % (self.dataset_name, data_type))

        self.n_batches = int(min(len(path_A), len(path_B)) / batch_size)
        total_samples = self.n_batches * batch_size

        # Sample n_batches * batch_size from each path list so that model sees all
        # samples from both domains
        path_A = np.random.choice(path_A, total_samples, replace=False)
        path_B = np.random.choice(path_B, total_samples, replace=False)

        for i in range(self.n_batches - 1):
            batch_A = path_A[i * batch_size:(i + 1) * batch_size]
            batch_B = path_B[i * batch_size:(i + 1) * batch_size]
            imgs_A, imgs_B = [], []
            for img_A, img_B in zip(batch_A, batch_B):
                img_A = self.imread(img_A)
                img_B = self.imread(img_B)

                img_A = scipy.misc.imresize(img_A, self.img_res)
                img_B = scipy.misc.imresize(img_B, self.img_res)

                if not is_testing and np.random.random() > 0.5:
                    img_A = np.fliplr(img_A)
                    img_B = np.fliplr(img_B)

                imgs_A.append(img_A)
                imgs_B.append(img_B)

            imgs_A = np.array(imgs_A) / 127.5 - 1.
            imgs_B = np.array(imgs_B) / 127.5 - 1.

            yield imgs_A, imgs_B

    def load_img(self, path):
        img = self.imread(path)
        img = scipy.misc.imresize(img, self.img_res)
        img = img / 127.5 - 1.
        return img[np.newaxis, :, :, :]

    def imread(self, path):
        return scipy.misc.imread(path, mode='RGB').astype(np.float)
        
        
# spherical linear interpolation (slerp)
def slerp(val, low, high):
	omega = np.arccos(np.clip(np.dot(low/np.linalg.norm(low), high/np.linalg.norm(high)), -1, 1))
	so = np.sin(omega)
	if so == 0:
		# L'Hopital's rule/LERP
		return (1.0-val) * low + val * high
	return np.sin((1.0-val)*omega) / so * low + np.sin(val*omega) / so * high
 
# uniform interpolation between two points in latent space
def interpolate_points(p1, p2, n_steps, zoom = 1.0):
	# interpolate ratios between the points
	ratios = np.linspace(0.5 - 0.5/zoom, 0.5 + 0.5 / zoom, num=n_steps)
	# linear interpolate vectors
	vectors = list()
	for ratio in ratios:
		v = slerp(ratio, p1, p2)
		vectors.append(v)
	return np.asarray(vectors)


def morph_images(start_z, end_z, gan_f_z, num_hybrids = 7, zoom = 1.0, mode='linear'):

    factors = np.linspace(0.5 - 0.5/zoom, 0.5 + 0.5 / zoom, num_hybrids+2, endpoint = True)

    z_spherical = interpolate_points(start_z, end_z, (num_hybrids +2), zoom)

    fig = plt.figure(figsize=(40, 24))
    fig.subplots_adjust(hspace=0, wspace=0)

    for i, factor in enumerate(factors):

        if mode == 'linear':
            changed_z_point = start_z * factor + end_z  * (1 - factor)
        else:
            changed_z_point = z_spherical[i]

        changed_image = ((gan_f_z(np.array([changed_z_point]))+1.)/2.)[0]

        img = changed_image.squeeze()
        sub = fig.add_subplot(1, len(factors)+1, i+1)
        sub.axis('off')
        sub.title.set_text('f : ' + str(round(factor,4))) 
        sub.imshow(img)

    plt.show()
    return fig


def make_hybrid(start_z, end_z, gan_f_z, mode, frac, hfn):

    z = slerp(frac, end_z, start_z)
    if mode == 'linear':
        z = start_z * frac + end_z  * (1 - frac)

    changed_image = ((gan_f_z(np.array([z]))+1.)/2.)[0]
    img = changed_image.squeeze()

    fig, axs = plt.subplots(1, 1, figsize=(9, 9))
    fig.subplots_adjust(left=0,right=1,bottom=0,top=1)
    fig.subplots_adjust(wspace=0, hspace=0)
    axs.imshow(img, cmap='gray_r')
    axs.axis('off')
    axs.axis('tight')

    fig.savefig(hfn)

    plt.show()
    

def film_z_morph(start_z, end_z, gan_f_z, filename, num_frames=500, mode='linear', framesps = 20, zoom = 1, outro = 60):
    zoom = 1
    fps = framesps

    factors = np.linspace(0.5 - 0.5/zoom, 0.5 + 0.5/zoom, num_frames + 2, endpoint = True)
    
    z_spherical = interpolate_points(start_z, end_z, (num_frames + 2), 1)
    
    image = ((gan_f_z(np.array([start_z]))+1.)/2.).squeeze()
    image = np.clip(image, 0, 1)
    image = np.uint8(255. * image)
    
    size = (image.shape[0],image.shape[1])
    #out = cv2.VideoWriter(filename, cv2.VideoWriter_fourcc(*'DIVX'), fps, size)
    out = cv2.VideoWriter(filename, cv2.VideoWriter_fourcc(*'MP4V'), fps, size)
    
    for i, factor in enumerate(factors):
        if mode == 'linear':
            changed_z_point = start_z * factor + end_z  * (1 - factor)
        else:
            changed_z_point = z_spherical[i]
        
        image = ((gan_f_z(np.array([changed_z_point]))+1.)/2.).squeeze()
        image = np.clip(image, 0, 1)
        image = np.uint8(255. * image)
        out.write(cv2.cvtColor(image, cv2.COLOR_RGB2BGR))
        if i == len(factors) - 1:
            for j in range(outro):
                imageOut = image
                out.write(cv2.cvtColor(imageOut, cv2.COLOR_RGB2BGR))
    
    out.release()

def film_z_explore(start_z, z, gan_f_z, filename, zrange = [-1.0, 1.0], num_frames = 100, mode='linear', framesps = 20):

    end_z = np.copy(start_z)

    for zt in z:
        start_z[zt] = start_z[zt] + zrange[0]
        end_z[zt] = end_z[zt] + zrange[1]

    film_z_morph(start_z, end_z, gan_f_z, filename, num_frames, mode, framesps)
        


def load_model(model_class, folder):
    with open(os.path.join(folder, 'params.pkl'), 'rb') as f:
        params = pickle.load(f)

    model = model_class(*params)

    model.load_weights(os.path.join(folder, 'weights/weights.h5'))

    return model


def load_model_pararms(model_class, folder, param_file):
    with open(os.path.join(folder, param_file), 'rb') as f:
        params = pickle.load(f)

    model = model_class(*params)

    model.load_weights(os.path.join(folder, 'weights/weights.h5'))

    return model


def load_mnist():
    (x_train, y_train), (x_test, y_test) = mnist.load_data()

    x_train = x_train.astype('float32') / 255.
    x_train = x_train.reshape(x_train.shape + (1,))
    x_test = x_test.astype('float32') / 255.
    x_test = x_test.reshape(x_test.shape + (1,))

    return (x_train, y_train), (x_test, y_test)


def load_mnist_gan():
    (x_train, y_train), (x_test, y_test) = mnist.load_data()

    x_train = (x_train.astype('float32') - 127.5) / 127.5
    x_train = x_train.reshape(x_train.shape + (1,))
    x_test = (x_test.astype('float32') - 127.5) / 127.5
    x_test = x_test.reshape(x_test.shape + (1,))

    return (x_train, y_train), (x_test, y_test)


def load_fashion_mnist(input_rows, input_cols, path='./data/fashion/fashion-mnist_train.csv'):
    # read the csv data
    df = pd.read_csv(path)
    # extract the image pixels
    X_train = df.drop(columns=['label'])
    X_train = X_train.values
    X_train = (X_train.astype('float32') - 127.5) / 127.5
    X_train = X_train.reshape(X_train.shape[0], input_rows, input_cols, 1)
    # extract the labels
    y_train = df['label'].values

    return X_train, y_train


def load_safari(folder):
    mypath = os.path.join("./data", folder)
    txt_name_list = []
    for (dirpath, dirnames, filenames) in walk(mypath):
        for f in filenames:
            if f != '.DS_Store':
                txt_name_list.append(f)
                break

    slice_train = int(80000 / len(txt_name_list))  ###Setting value to be 80000 for the final dataset
    i = 0
    seed = np.random.randint(1, 10e6)

    for txt_name in txt_name_list:
        txt_path = os.path.join(mypath, txt_name)
        x = np.load(txt_path)
        x = (x.astype('float32') - 127.5) / 127.5
        # x = x.astype('float32') / 255.0

        x = x.reshape(x.shape[0], 28, 28, 1)

        y = [i] * len(x)
        np.random.seed(seed)
        np.random.shuffle(x)
        np.random.seed(seed)
        np.random.shuffle(y)
        x = x[:slice_train]
        y = y[:slice_train]
        if i != 0:
            xtotal = np.concatenate((x, xtotal), axis=0)
            ytotal = np.concatenate((y, ytotal), axis=0)
        else:
            xtotal = x
            ytotal = y
        i += 1

    return xtotal, ytotal


def load_cifar(label, num):
    if num == 10:
        (x_train, y_train), (x_test, y_test) = cifar10.load_data()
    else:
        (x_train, y_train), (x_test, y_test) = cifar100.load_data(label_mode='fine')

    train_mask = [y[0] == label for y in y_train]
    test_mask = [y[0] == label for y in y_test]

    x_data = np.concatenate([x_train[train_mask], x_test[test_mask]])
    y_data = np.concatenate([y_train[train_mask], y_test[test_mask]])

    x_data = (x_data.astype('float32') - 127.5) / 127.5

    return (x_data, y_data)


def load_semper(data_folder, image_size, batch_size):

    data_gen = ImageDataGenerator(preprocessing_function=lambda x: (x.astype('float32') - 127.5) / 127.5)

    x_train = data_gen.flow_from_directory(data_folder
                                           , target_size=(image_size, image_size)
                                           , batch_size=batch_size
                                           , shuffle=True
                                           , class_mode='input'
                                           , subset="training"
                                           )

    return x_train
    

def load_semper_aug(data_folder, image_size, batch_size, zoom):
    
    data_gen = ImageDataGenerator(preprocessing_function=lambda x: (x.astype('float32') - 127.5) / 127.5
                                        	, rotation_range=15
                                        	, zoom_range=zoom
                                        	, shear_range=0.05
                                        	, horizontal_flip=True
                                            , vertical_flip=False
                                        	, fill_mode="nearest")

    x_train = data_gen.flow_from_directory(data_folder
                                           , target_size=(image_size, image_size)
                                           , batch_size=batch_size
                                           , shuffle=True
                                           , class_mode='input'
                                           , subset="training"
                                           )

    return x_train


def load_music(data_name, filename, n_bars, n_steps_per_bar):
    file = os.path.join("./data", data_name, filename)

    with np.load(file, encoding='bytes') as f:
        data = f['train']

    data_ints = []

    for x in data:
        counter = 0
        cont = True
        while cont:
            if not np.any(np.isnan(x[counter:(counter + 4)])):
                cont = False
            else:
                counter += 4

        if n_bars * n_steps_per_bar < x.shape[0]:
            data_ints.append(x[counter:(counter + (n_bars * n_steps_per_bar)), :])

    data_ints = np.array(data_ints)

    n_songs = data_ints.shape[0]
    n_tracks = data_ints.shape[2]

    data_ints = data_ints.reshape([n_songs, n_bars, n_steps_per_bar, n_tracks])

    max_note = 83

    where_are_NaNs = np.isnan(data_ints)
    data_ints[where_are_NaNs] = max_note + 1
    max_note = max_note + 1

    data_ints = data_ints.astype(int)

    num_classes = max_note + 1

    data_binary = np.eye(num_classes)[data_ints]
    data_binary[data_binary == 0] = -1
    data_binary = np.delete(data_binary, max_note, -1)

    data_binary = data_binary.transpose([0, 1, 2, 4, 3])

    return data_binary, data_ints, data


def preprocess_image(data_name, file, img_nrows, img_ncols):
    image_path = os.path.join('./data', data_name, file)

    img = load_img(image_path, target_size=(img_nrows, img_ncols))
    img = img_to_array(img)
    img = np.expand_dims(img, axis=0)
    img = vgg19.preprocess_input(img)
    return img
    
    
def load_images_local(src, dst):
    try:
        shutil.copytree(src, dst)
    except OSError as exc: # python >2.5
        if exc.errno == errno.ENOTDIR:
            shutil.copy(src, dst)
        else: raise
    
    
def load_and_process_img(path_to_img):
    img = load_img(path_to_img)
    img = tf.keras.applications.vgg19.preprocess_input(img)
    return img
  
  
def deprocess_img(processed_img):
    x = processed_img.copy()
    if len(x.shape) == 4:
        x = np.squeeze(x, 0)
    assert len(x.shape) == 3, ("Input to deprocess image must be an image of "
                             "dimension [1, height, width, channel] or [height, width, channel]")
    if len(x.shape) != 3:
        raise ValueError("Invalid input to deprocessing image")
    
    # perform the inverse of the preprocessiing step
    x[:, :, 0] += 103.939
    x[:, :, 1] += 116.779
    x[:, :, 2] += 123.68
    x = x[:, :, ::-1]
    
    x = np.clip(x, 0, 255).astype('uint8')
    return x
    
    
def a1sort(data):
    return sorted(data, key=lambda item: (int(item.partition(' ')[0])
                               if item[0].isdigit() else float('inf'), item))
                               
                              
def check_zs_zeros(zs_logfn):
    next_z = 0
    if os.path.exists(zs_logfn):
        print('found file of recovered z values')
        zs_check = np.load(zs_logfn)
        for j in range(zs_check.shape[0]):
            all_zeros = not np.any(zs_check[j])
            if all_zeros:
                next_z = j
                print('found zero z value at index {}'.format(next_z))
                break
    return next_z
    
        
def plottwo(fig1, fig2, scale=20):
        fig = plt.figure(figsize=(scale, scale/2))
        fig.subplots_adjust(hspace=0, wspace=0)
        sub = fig.add_subplot(1, 2, 1)
        sub.axis('off')
        sub.imshow(fig1)
        sub = fig.add_subplot(1, 2, 2)
        sub.axis('off')
        sub.imshow(fig2)
        plt.show()